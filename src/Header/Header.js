import React from "react";
import {AppBar, Typography, Toolbar} from "@mui/material";
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import NotificationsIcon from '@mui/icons-material/Notifications';
import bis from '../assets/Bis.png';
import Box from '@mui/material/Box';
// import { makeStyles } from '@mui/styles';
import { createTheme , ThemeProvider } from '@mui/material';
import { grey } from '@mui/material/colors';


const theme = createTheme({
    typography: {
        fontFamily: [
            'Montserrat',
            'sans-serif',
        ].join(','),
    },
    palette: {
        primary: {
            main: grey[700],
        },
    },
});

const Header = () => {
  
    return (
        <React.Fragment>
            <ThemeProvider theme={theme}>
                <AppBar color='transparent'position='static'>
                    <Toolbar>
                        <AccountCircleIcon fontSize='large' sx={{width: 56, height: 56}}/>
                        <Typography align="center" sx={{marginLeft: 1}}>Remy Sharp</Typography>
                        <Box component={'img'} src={bis} sx={{marginLeft: 50, marginRight:1, width: 60, height: 60}} /> 
                        <Typography align="center" color='primary'>Driver App</Typography>
                            <NotificationsIcon fontSize='medium' sx={{marginLeft: 'auto'}}/>   
                    </Toolbar>
                </AppBar>
            </ThemeProvider>
        </React.Fragment>
    );
};

export default Header;
