import React from 'react';
import { TableFooter, Typography } from '@mui/material'; 
import Box from '@mui/material/Box';
import { createTheme , ThemeProvider } from '@mui/material';
import driver from '../assets/chauffeur.png';
import Grid from '@mui/material/Grid';
import briefing from '../assets/briefing.png';
import building from '../assets/building.png';
import notes from '../assets/notes.png';
import serah from '../assets/serahterima.png';
import WarningAmberRoundedIcon from '@mui/icons-material/WarningAmberRounded';
import { grey } from '@mui/material/colors';
import Button from '@mui/material/Button';
// bikin tombol search
import Paper from '@mui/material/Paper';
import InputBase from '@mui/material/InputBase';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
// import MenuIcon from '@mui/icons-material/Menu';
import SearchIcon from '@mui/icons-material/Search';

import { Container } from '@mui/system';

// bagian ngubah tanggal (iconnya)
import KeyboardArrowLeftIcon from '@mui/icons-material/KeyboardArrowLeft';
import KeyboardArrowRightIcon from '@mui/icons-material/KeyboardArrowRight';
import { TextField } from '@mui/material';
// membuat table
import { TableContainer, Table, TableHead, TableBody, TableRow, TableCell } from '@mui/material';
import NoteOutlinedIcon from '@mui/icons-material/NoteOutlined';
import TablePagination from '@mui/material/TablePagination';
import Stack from '@mui/material/Stack';
import Pagination from '@mui/material/Pagination';
import { useState } from 'react';




 
const theme = createTheme({
    palette: {
        primary: {
            main : grey[200],
        },
        // primary: lightBlue,
        // secondary: pink,
    }
});


function Menu() {

    // const [page, setPage] = React.useState(1);
    // const [rowsPerPage, setRowsPerPage] = React.useState(1);
  
    // const handleChangePage = (event, newPage) => {
    //   setPage(newPage);
    // };
  
    // const handleChangeRowsPerPage = (event) => {
    //   setRowsPerPage(+event.target.value);
    //   setPage(0);
    // };

    // state nama
    // const [nama, setnama] = React.useState('NOVAL,BANA,GHATAN');
    // const [quantyty, setquantyty] = React.useState(0)
    // const [harga, setHarga] = React.useState(1200000)


    // untuk panigation yang dibawah
    const [page, setPage] = React.useState(1);
    // const rowsPerPage = 3;
  
    const handleChange = (event, value) => {
      setPage(value);
    };
 
    // membuat filter table
    const [filter, setFilter] = useState('');
    const [rowsPerPage, setRowsPerPage] = useState(3);

    const filteredData = tableData.filter(row =>
        row.bulan.toLowerCase().includes(filter.toLowerCase())
      );

    const handleChangeRowsPerPage = event => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(1);
      };
    
    const handleBackButtonClick = (event) => {
        setPage(page - 1);
      };
    
    const handleNextButtonClick = (event) => {
        setPage(page + 1);
      };

    return (
        <ThemeProvider theme={theme}>
            <Box bgcolor='#303F51'>
                    <Container maxWidth="xl">
                        <Typography sx={{bgColor: 'primary'}} color='#FFFFFF'>Jadwal Operasional Supir / Riwayat Catatan</Typography>
                    </Container>
                    <Box>
                        <Grid container spacing={1} justifyContent='center'>
                            <Button>   
                            <Grid item>
                                <Box component={'img'} src={driver} sx={{width: 50 , height: 50 , margin: 4}}/>
                                    <Typography variant='subtitle2'>Rute Operasional</Typography>
                                   
                            </Grid>
                            <Divider sx={{ height: 60, m: 0.5, marginTop: 4, borderColor: 'white'}} orientation="vertical" flexItem />
                            </Button>
                            
                            <Button> 
                            <Grid item>
                                <Box component={'img'} color='primary'src={briefing} sx={{width: 50 , height: 50 , margin: 4}}/>
                                <Typography variant='subtitle2'>Briefing</Typography>
                            </Grid>
                            <Divider sx={{ height: 60, m: 0.5, marginTop: 4, borderColor: 'white'}} orientation="vertical" flexItem />
                            </Button>

                            <Button>
                            <Grid item>
                                <Box component={'img'} src={building} sx={{width: 50 , height: 50 , margin: 4}}/>
                                <Typography variant='subtitle2'>Go/No Go Item</Typography>
                            </Grid> 
                            <Divider sx={{ height: 60, m: 0.5, marginTop: 4, borderColor: 'white'}} orientation="vertical" flexItem />
                            </Button>

                            <Button>  
                            <Grid item>
                                <Box component={'img'} src={notes} sx={{width: 50 , height: 50 , margin: 4}}/>
                                <Typography variant='subtitle2' color='#ECAB55'>Riwayat Catatan</Typography>
                            </Grid>   
                            <Divider sx={{ height: 60, m: 0.5, marginTop: 4, borderColor: 'white'}} orientation="vertical" flexItem />

                            </Button>

                            <Button>
                            <Grid item>
                                <Box component={'img'} src={serah} sx={{width: 50 , height: 50 , margin: 4}}/>
                                <Typography variant='subtitle2'>Serah Terima Dinasan</Typography>
                               
                            </Grid>  
                            <Divider sx={{ height: 60, m: 0.5, marginTop: 4, borderColor: 'white'}} orientation="vertical" flexItem />
                            </Button>
                            
                            <Button> 
                            <Grid item>
                                <WarningAmberRoundedIcon color='primary'sx={{width: 60 , height: 60 , margin: 3.5}} />
                                <Typography variant='subtitle2'>Bantuan Darurat</Typography>
                            </Grid>
                            </Button>
                        </Grid>
                    </Box>
                       
                        <Grid container justifyContent='center'>
                            {/* tombol search */}
                            <Paper component="form" sx={{ p: '2px 4px', display: 'flex', alignItems: 'center', width: 400, marginTop: 2 }}>
                                {/* <IconButton sx={{ p: '10px' }} aria-label="menu">
                                    <MenuIcon />
                                </IconButton> */}
                                <InputBase
                                    sx={{ ml: 1, flex: 1 }}
                                    placeholder="Pencarian"
                                    inputProps={{ 'aria-label': 'Pencarian' }}
                                />
                                    <IconButton type="button" sx={{ p: '10px' }} aria-label="search">
                                        <SearchIcon/>
                                    </IconButton>

                            </Paper>
                        </Grid>

                    <Box>
                        <Typography 
                        sx={{fontFamily: 'Montserrat', marginTop: 4}} 
                        color='white'
                        align='center'
                        >RIWAYAT CATATAN OPERASIONAL</Typography> 
                    </Box>   
                    

                    <Container maxWidth="xl">
                    {/* Bagian Select Bulan */}
                    <Box display='flex' justifyContent='center' pt={2}>
                        <Box>
                            <Button onClick={handleBackButtonClick} variant="contained" sx={{backgroundColor: '#313541', width: 40, height: 40}}>
                            <KeyboardArrowLeftIcon fontSize="medium"/>
                            </Button>
                        </Box>
                        <Box>
                            <TextField
                              value={filter}
                              onChange={e => setFilter(e.target.value)}

                              id="outlined-read-only-input"
                              defaultValue="Desember 2022"
                              InputProps={{  
                                  inputProps: { 
                                    style: { textAlign: "center", width: 269, height: 8 }
                                }
                              }}
                              sx={{backgroundColor: 'white', color: 'black'}}
                            />
                        </Box>
                        <Box>
                            <Button onClick={handleNextButtonClick} variant="contained" sx={{backgroundColor: '#313541', width: 40, height: 40}}>
                            <KeyboardArrowRightIcon fontSize="medium"/>
                            </Button>
                        </Box>
                    </Box>
                    <Divider sx={{borderColor: 'white', pt: 2}}/> 


                    {/* mumbuat table */}
                    <TableContainer component={Paper}>
                        <Table arial-label='simple table'>
                            <TableHead sx={{backgroundColor: '#303F51'}}>
                                <TableRow>
                                    <TableCell sx={{color: 'white'}}>TANGGAL</TableCell>
                                    <TableCell sx={{color: 'white'}} align='center'>WAKTU</TableCell>
                                    <TableCell sx={{color: 'white'}} align='left' >TRAYEK</TableCell>
                                    <TableCell sx={{color: 'white'}} align='left'>NO. BUS</TableCell>
                                    <TableCell></TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {filteredData.slice((page - 1) * rowsPerPage,(page - 1) * rowsPerPage + rowsPerPage).map(row => (
                                    <TableRow 
                                        key={row.tanggal}
                                        sx={{'&:last-child td, &:last-child th' : { border: 0}}}
                                    >
                                        <TableCell align='left' width={140}>{row.tanggal}</TableCell>
                                        <TableCell align='center' width={90}>{row.waktu}</TableCell>
                                        <TableCell align='left' width={270}>{row.trayek}</TableCell>
                                        <TableCell align='left'>{row.no_bus}</TableCell>
                                        <TableCell align='center' bgColor='#28A745' sx={{width: 157}}>
                                            
                                            <Button
                                                // variant="contained"
                                                size='large'
                                                // color="success"
                                                sx={{
                                                    mr: 1, 
                                                    width: 110,
                                                    height: '100%',
                                                    "&:hover": { backgroundColor: "28A745"},
                                                    fontFamily: 'roboto',
                                                }}
                                            >
                                            <NoteOutlinedIcon  fontSize="large"/>
                                            <Typography sx={{ ml: 1 , fontFamily: 'roboto', fontSize: 10, textAlign: 'left' }}>      
                                            Lihat
                                            Catatan
                                            </Typography>
                                            </Button> 
                                
                                        </TableCell>
                                    </TableRow>
                              
                                ))
                                }
                            </TableBody>
                        </Table>
                    </TableContainer>
                
                    <Box pt={10}>
                    <Stack spacing={2} alignItems='center'>
                        <Pagination 
                        //  count={5} 
                         shape="rounded"
                         count={Math.ceil(tableData.length / rowsPerPage)}
                         page={page}
                         onChange={handleChange}
                            sx={{
                                "& .MuiPaginationItem-page": { color: "white", border: "1px solid white" },
                                "& .Mui-selected": { backgroundColor: "#00FF00" },
                                "& .MuiPagination-ul li:last-child button::before": {content: "'Next'", marginRight: "12px", color: 'white'},
                                "& .MuiPagination-ul li:first-child button::after": {content: "'Prev'", marginLeft: "12px", color: 'white'},
                            }}
                        />
                    </Stack>
                    </Box>


                    {/* <Box>
                        <Typography>{harga * quantyty}</Typography>
                        <Button disabled={quantyty < 1 ? true:false} onClick={()=>setquantyty(quantyty - 1)}>-</Button>
                        <TextField
                              value={quantyty}
                              onChange={e => setquantyty(e.target.value)}

                              id="outlined-read-only-input"
                              defaultValue="Desember 2022"
                              InputProps={{  
                                  inputProps: { 
                                    style: { textAlign: "center", width: 269, height: 8 }
                                }
                              }}
                              sx={{backgroundColor: 'white', color: 'black'}}
                        />
                        <Button onClick={()=>setquantyty(quantyty + 1)}>+</Button>
                    </Box> */}

                    </Container>                   
            </Box>
        </ThemeProvider>
    );
}

export default Menu;

const tableData = [{
    "bulan": "Desember",
    "tanggal": "Jumat, 30 Des 2022",
    "waktu": "06:00 WIB",
    "trayek": "TERM. PONOROGO - KALIDERES",
    "no_bus": "BM007",
  }, {
    "bulan": "Desember",
    "tanggal": "Rabu, 28 Des 2022",
    "waktu": "06:20 WIB",
    "trayek": "KALIDERES - TERM. PONOROGO",
    "no_bus": "BM007",
  }, {
    "bulan": "Desember",
    "tanggal": "Selasa, 27 Des 2022",
    "waktu": "07:00 WIB",
    "trayek": "WONOGIRI - TERM. PINANG RANTI",
    "no_bus": "BM023",
  },
  { 
    "bulan": "Janurai",
    "tanggal": "Senin, 1 Jan 2023",
    "waktu": "06:00 WIB",
    "trayek": "KALIDERES - TERM. PONOROGO",
    "no_bus": "BM023",
  }, {
    "bulan": "Januari",
    "tanggal": "Kamis, 10 Jan 2023",
    "waktu": "06:20 WIB",
    "trayek": "KALIDERES - TERM. PONOROGO",
    "no_bus": "BM007",
  }, {
    "bulan": "Januari",
    "tanggal": "Sabtu, 20 Jan 2023",
    "waktu": "08:00 WIB",
    "trayek": "KALIDERES - TERM. PONOROGO",
    "no_bus": "BM007",
  }, {
    "bulan": "Januari",
    "tanggal": "Minggu, 28 Jan 2023",
    "waktu": "06:00 WIB",
    "trayek": "WONOGIRI - TERM. PINANG RANTI",
    "no_bus": "BM023",
  }, {
    "bulan": "Januari",
    "tanggal": "Selasa, 21 Jan 2023",
    "waktu": "05:20 WIB",
    "trayek": "WONOGIRI - TERM. PINANG RANTI",
    "no_bus": "BM023",
  }, {
    "bulan": "Januari",
    "tanggal": "Kamis, 31 Jan 2023",
    "waktu": "07:00 WIB",
    "trayek": "WONOGIRI - TERM. PINANG RANTI",
    "no_bus": "BM023",
  },
  {
    "bulan": "Februari",
    "tanggal": "Senin, 3 Feb 2023",
    "waktu": "05:00 WIB",
    "trayek": "WONOGIRI - TERM. PINANG RANTI",
    "no_bus": "BM023",
  }, {
    "bulan": "Februari",
    "tanggal": "Selasa, 4 Feb 2023",
    "waktu": "05:20 WIB",
    "trayek": "KALIDERES - TERM. PONOROGO",
    "no_bus": "BM023",
  }, {
    "bulan": "Februari",
    "tanggal": "Rabu, 5 Feb 2023",
    "waktu": "05:00 WIB",
    "trayek": "TERM. PONOROGO - KALIDERES",
    "no_bus": "BM023",
  },
  {
    "bulan": "Februari",
    "tanggal": "Kamis, 11 Feb 2023",
    "waktu": "08:00 WIB",
    "trayek": "WONOGIRI - TERM. PINANG RANTI",
    "no_bus": "BM007",
  }, {
    "bulan": "Februari",
    "tanggal": "Jumat, 12 Feb 2023",
    "waktu": "08:10 WIB",
    "trayek": "KALIDERES - TERM. PONOROGO",
    "no_bus": "BM007",
  }, {
    "bulan": "Februari",
    "tanggal": "Sabtu, 13 Feb 2023",
    "waktu": "08:13 WIB",
    "trayek": "TERM. PONOROGO - KALIDERES",
    "no_bus": "BM023",
  }]